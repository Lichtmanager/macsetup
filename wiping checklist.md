Things to do before wiping the OS and all Data

1. Back Up
- do time machine backup

2. Copy folders needed directly after reinstall
- copy "Developement" folder to NAS
- copy VPN folder to NAS

3. Prepare Bootstick
- format bootstick. 
    https://support.apple.com/de-de/HT201372
- download OS Installer
- write OS to flash drive

4. Wipe
- wipe mac completely
- reinstall OS from Bootstick

5. SetUp Mac
- SetUp iCloud
- copy important folders (see above)
- go to software list. happy installing :)
