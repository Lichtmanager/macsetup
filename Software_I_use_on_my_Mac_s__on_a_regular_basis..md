 # Software I use on my Mac(s) on a regular basis. 
 




- VPN & Remote
    - ExpressVPN
    - Tunnelblick
    - Team Viewer
    - Remote Desktop
    - WireGuard

- Kommunikation
    - Webex
    - Teams
    - Telegram
    - Zoom
    - Whatsapp 
    - Stashcat
    - Signal
    - Slack
    - Skype
    - Discord
    
- Browser 
    - Firefox
    - Chrome

- System Tools
    - Mosaic
    - Alfred
    - Amphetamine
    - SyncThing
    - NTFS Tuxera
    - iStats

- Entwicklerwerkzeuge
    - JetBrains Toolbox
    - Docker
    - VMWare Fusion
    - FileZilla
    - VNC Viewer
    - Balena Etcher
    - xBar
    - iStat Menus
    - Tower Git
    - Wireshark
    - brew

- Video & Musik
    - Spotify
    - OBS
    - Steelseries Engine 3
    - VLC

- Office 
    - Goodnotes
    - MS Office
    - Slack
    - 

- Cloud 
    - One Drive
    - Next Cloud
    - OneDrive

- Misc
    - drwa.io
    - Tiled
    - Moodle Desktop
    - Vera Crypt
    - Minecraft
    - Adblockplus
    - Breitbandmessung
    - Minecraft 
    - AdBlock Plus
    
    
